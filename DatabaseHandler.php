<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 11.07.2018
 * Time: 23:11
 */

class DatabaseHandler
{

    public $databaseLink;

    public function __construct ( $host = 'localhost', $username = 'root', $password = '', $database = '' ) {
        if( !($this->databaseLink = mysqli_connect( $host, $username, $password, $database) ) ){
            throw new LogicException("Verbindung mit der Datenbank konnte nicht hergestellt werden, Fehlercode: " . mysqli_error ( $this->databaseLink ));
        }
    }

    public function __destruct(){
        if( ! mysqli_close($this->databaseLink) ){
            throw new LogicException("Verbindung konnte nicht geschlossen werden, Fehlercode: " . mysqli_error ( $this->databaseLink ));
        }
    }

    /*
     *
     *
     */
    public function select( $what, $table, $conditions, $order, $joins) {

        // Prüfen ob $table Leer ist
        if ( $table != '' ) {

            // Query bauen
            $query = "SELECT ";
            if ($what == '') {
                $query .= "* ";
            } else {
                $query .= $what." ";
            }

            $query .= "FROM ".$table." ";

            // Joins
            if ($joins != '') {
                foreach($joins as $join) {
                    $query .= "LEFT JOIN ".$join." ";
                }
            }

            // WHERE Sachen
            if ( $conditions != '') {
                $query .= 'WHERE ';
                foreach($conditions as $cond) {
                    $query .= $cond." AND ";
                }
                $query .= " ";
                $query = substr($query, 0,-5);
            }


            if ( $order != '') {
                $query .= "ORDER BY ".$order;
            }

            // Query ausführen
            return $this->execute($query);


        } else {
            throw new LogicException("Keine Tabelle angegeben");
        }


    }

    public function insert($table, $variables, $values) {

        if ( $table != '' ) {

            // Query bauen
            $query = "INSERT INTO ";
            $query .= $table." ";

            if( $variables != '' ) {
                $query .= "(";
                foreach ($variables as $var) {
                    $query .= "`".mysqli_real_escape_string($this->databaseLink,$var)."`,";
                }
                $query .= ")";
            } else {
                throw new LogicException("Keine Variablen ausgewählt");
            }
            $query = substr($query, 0,-2);
            $query .= ") VALUES ";

            if( $values != '' ) {
                $query .= "(";
                foreach ($values as $val) {
                    $query .= "'".mysqli_real_escape_string($this->databaseLink,$val)."',";
                }
                $query .= ")";
            } else {
                throw new LogicException("Keine Werte angegeben");
            }

            $query = substr($query, 0,-2);
            $query .= ")";
            // Query ausführen
            return $this->execute($query);

        } else {
            throw new LogicException("Keine Tabelle angegeben");
        }

    }

    public function update($table, $fields, $variables, $where) {
        if ( $table != '' ) {

            // Query bauen
            $query = "UPDATE ";
            $query .= $table." SET ";

            $fieldarray = array();
            $vararray = array();

            if( $fields != '' ) {
                foreach($fields as $fiel) {
                    array_push($fieldarray, $fiel);
                }
            } else {
                throw new LogicException("Keine Felder eingegeben");
            }

            if($variables != '') {
                foreach($variables as $var) {
                    array_push($vararray, $var);
                }
            } else {
                throw new LogicException("Keine Werte eingegeben");
            }

            // Felder und Variablen vereinen
            $i = 0;
            foreach($fieldarray as $fieldname) {
                if ( $vararray[$i] != '' ) {
                    $query .= $fieldname."='".$vararray[$i]."', ";
                } else {
                    $query .= $fieldname." = '', ";
                }
            }

            // letztes Komma abschneiden
            $query = substr($query, 0, -2);

            if ( $where != '') {
                $query .= ' WHERE ';
                foreach($where as $whe) {
                    $query .= "".$whe." AND ";
                }
                $query .= " ";
                $query = substr($query, 0,-5);
            }

            // Query ausführen
            return $this->execute($query);

        } else {
            throw new LogicException("Keine Tabelle angegeben");
        }
    }

    public function deleteFrom($table,$where) {
        if ( $table != '' ) {

            $query = "DELETE FROM ";
            $query .= $table." ";

            if ( $where != '') {
                $query .= 'WHERE ';
                foreach($where as $whe) {
                    $query .= $whe." AND ";
                }
                $query .= " ";
                $query = substr($query, 0,-5);
            }

            return $this->execute($query);

        } else {
        throw new LogicException("Keine Tabelle angegeben");
        }
    }

    private function execute($query) {
        // Prüfen ob $query leer
        if ( $query != '') {

            // return mysqli query
            if ( $result = mysqli_query($this->databaseLink,$query) ) {
                return $result;
            } else {
                throw new LogicException("Query Fehler: ".mysqli_error($this->databaseLink));
            }

        } else {
            throw new LogicException("Keine Query übergeben");
        }
    }

}