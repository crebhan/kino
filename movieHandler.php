<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 15.07.2018
 * Time: 01:09
 */

class movieHandler
{

    public $database;
    
    public function __construct() {
        $this->database = new DatabaseHandler('localhost','root','','kino');
    }

    public function addMovie($title, $img, $fsk, $length, $dimension, $availableuntil) {
        $this->database->insert("filme",array("title","image","fsk","length","dimension","availableUntil"),array($title,$img,$fsk,$length,$dimension,$availableuntil));
    }

    public function addVorstellung($filmID, $kinoID, $seats, $vars) {
        // Variablen durchgehen und Vorstellungen in die Datenbank eintragen


        foreach( $vars as $dat => $var) {
            //[3_21_07] => on
            $tmp = explode("_",$dat);
            $date = date("Y")."-".$tmp[2]."-".$tmp[1];

            $uhrzeit = $tmp[0];

            $data = $this->database->select("*","vorstellungen",array("filmID='".$filmID."'","tag='".$date."'","zeit='".$uhrzeit."'"),"","");
            $tmp = mysqli_fetch_row($data);
            if ( $tmp[0] != '') {
                $this->database->deleteFrom("vorstellungen",array("ID=".$tmp[0]));
            }

            $this->database->insert("vorstellungen",array("kinoID","filmID","sitzplan","tag","zeit"),array($kinoID,$filmID,$seats,$date,$uhrzeit));
        }


    }

    public function addReservation($vID, $userID, $postVars, $seatarray) {

        $string = array(array());

        $seats = explode(";",$seatarray);
        $seatstring = "";

        $i = 0;
        $x = 0;

        foreach($seats as $rows) {
            $tmp = explode(",",$rows);

            foreach($tmp as $tmp2) {
                if ( $tmp2 != '' ) {
                    $string[$i][$x] = $tmp2;
                }
                $x++;
            }
            $x = 0;
            $i++;
        }

        foreach ($postVars as $k => $post) {
            if($k != 'submit' && $k != 'vID') {
                $seatstring .= $k.",";
                $tmp = explode("_",$k);
                $string[$tmp[0]][$tmp[1]] = "1";
            }
        }

        $seats = "";
        foreach($string as $str) {
            foreach($str as $st) {
                $seats .= $st.",";
            }
            $seats .= ";";

        }

        $this->database->update("vorstellungen",array("sitzplan"),array($seats),array("ID=".$vID));
        $this->database->insert("reservierungen",array("userID","vorstellungsID","seats"),array($userID,$vID,$seatstring));

    }

    public function deleteMovie($movieID) {
        // TODO: delete from Database
        $this->database->deleteFrom("filme",array("ID=".$movieID));
    }

}