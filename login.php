<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 12.07.2018
 * Time: 21:16
 */
session_start();
include("DatabaseHandler.php");
$database = new DatabaseHandler('localhost','root','','kino');
?>
<html>
<head>
    <title> Kino - Login </title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="loginpage.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
</head>
<body class="text-center bg-dark">
    <form class="form-signin secondNavbar" method="post" action="#">
        <?php
        if(isset($_POST["submit"])) {
            // User in Datenbank eintragen
            $res = $database->select("*","user",array("username='".$_POST["email"]."'"),"","");
            $tmp = mysqli_fetch_row($res);

            if (md5($_POST["password"]) == $tmp[2]) {
                // Login
                $_SESSION['user'] = $tmp[0];
                header("Location: index.php");
                #echo '<div class="alert alert-success">Erfolgreich eingeloggt</div><a href="index.php" class="btn btn-primary btn-block">Weiter zur Hauptseite</a>';

            } else {
                // Passwörter stimmen nicht überein oder user nicht verfügbar
                echo '<div class="alert alert-danger">Passwort falsch oder User existiert nicht</div><a href="login.php" class="btn btn-primary btn-block">zum Login</a>';
            }
        } else {
            ?>
            <i class="fas fa-ticket-alt" style="font-size: 50px;margin-bottom: 30px;color: #007bff;"></i>
            <label class="sr-only">Email address</label>
            <input type="email" name="email" class="form-control" placeholder="Email Adresse" required autofocus>
            <label class="sr-only">Password</label>
            <input type="password" name="password" class="form-control" placeholder="Passwort" required>
            <button class="btn btn-lg btn-primary btn-block" name="submit" type="submit">Einloggen</button>
            <a class="btn btn-lg btn-outline-dark btn-block" href="register.php">Registrieren</a>
            <p class="mt-5 mb-3 text-muted">&copy; 2018 Christoph Rebhan</p>
            <?php
        }
        ?>
    </form>
</body>
</html>
