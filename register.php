<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 15.07.2018
 * Time: 16:06
 */
session_start();
include("DatabaseHandler.php");
$database = new DatabaseHandler('localhost','root','','kino');
?>
<html>
<head>
    <title> Kino - Registrieren </title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="loginpage.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
</head>
<body class="text-center bg-dark">
<form class="form-signin secondNavbar" method="post" action="#">
    <?php

    if(isset($_POST["submit"])) {
        // User in Datenbank eintragen
        $res = $database->select("*","user",array("username='".$_POST["email"]."'"),"","");
        if ( $res->num_rows > 0 ) {
            // user existiert schon
            echo '<div class="alert alert-danger">Benutzer existiert bereits!</div><a href="register.php" class="btn btn-primary btn-block">zurück zur Registrierung</a>';
        } else {
            $database->insert("user",array("username","password"),array($_POST["email"],md5($_POST["password"])));
            echo '<div class="alert alert-success">Registrierung erfolgreich!</div><a href="login.php" class="btn btn-primary btn-block">zum Login</a>';
        }

        mysqli_free_result($res);

    } else {

        ?>
        <label class="sr-only">Email address</label>
        <input type="email" name="email" class="form-control" placeholder="Email Adresse" required autofocus>
        <label class="sr-only">Password</label>
        <input type="password" name="password" class="form-control" placeholder="Passwort" required>
        <button class="btn btn-lg btn-outline-primary btn-block" name="submit" type="submit">Registrieren</button>
        <?php
    }
    ?>
    <p class="mt-5 mb-3 text-muted">&copy; 2018 Christoph Rebhan</p>
</form>
</body>
</html>
