<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 15.07.2018
 * Time: 12:06
 */
?>
<div class="container">
    <div class="card box-shadow mb-4">
        <div class="card-header">
            Meine Reservierungen
        </div>
        <div class="card-body">
            <?php
                $res = $database->select("*","reservierungen",array("userID=".$_SESSION["user"]),"",array("vorstellungen ON reservierungen.vorstellungsID = vorstellungen.ID","filme ON vorstellungen.filmID = filme.ID"));
                if ($res->num_rows > 0) {
                    echo '<table class="table"><tr class="bg-dark text-light"><td>Film</td><td>Datum</td><td>Uhrzeit</td><td>Sitzplätze</td></tr>';
                    while ( $tmp = mysqli_fetch_row($res)) {
                        //Array ( [0] => 1 [1] => 1 [2] => 23 [3] => 2_2,3_2, [4] => 23 [5] => 1 [6] => 4 [7] => 0,0,0,0,0,; 0,0,0,0,0,; 0,0,1,0,0,; 0,0,1,0,-,; 0,0,0,0,-,; [8] => [9] => 2018-07-20 [10] => 2 [11] => 4 [12] => 1446584.jpg [13] => 103 [14] => 12 [15] => 2018-08-01 [16] => 3D [17] => Skyscraper )
                        if ($tmp[11] == "0" ) { $time = "14:00"; }
                        if ($tmp[11] == "1" ) { $time = "17:00"; }
                        if ($tmp[11] == "2" ) { $time = "20:00"; }
                        if ($tmp[11] == "3" ) { $time = "22:30"; }
                        echo '<tr><td>'.$tmp[17].'</td><td>'.$tmp[9].'</td><td>'.$time.'</td><td>'.$tmp[3].'</td></tr>';
                    }
                    echo '</table>';
                } else {
                    echo "- Keine Reservierungen gefunden -";
                }

                mysqli_free_result($res);
            ?>

        </div>
    </div>
</div>
