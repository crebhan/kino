<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 14.07.2018
 * Time: 16:46
 */

?>

<div class="container">
    <div class="card box-shadow mb-4">
        <div class="card-header">
            Neuen Kinofilm hinzufügen
        </div>
        <div class="card-body">
            <form method="post" action="?page=addmovie">
                <div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label>Filmtitel</label>
                            <input type="text" class="form-control" name="title" placeholder="Jurassic World" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Filmplakat (liegt in /img)</label>
                            <input type="text" class="form-control" name="img" placeholder="1131424.jpg" required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Altersfreigabe</label>
                            <select class="form-control" name="fsk">
                                <option>0</option>
                                <option>6</option>
                                <option selected>12</option>
                                <option>16</option>
                                <option>18</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Länge (in min)</label>
                            <input type="text" class="form-control" name="length" placeholder="120" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Dimension</label>
                            <select class="form-control" name="dimension">
                                <option selected>3D</option>
                                <option>2D</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Verfügbar bis</label>
                            <input type="text" class="form-control" name="availableuntil" placeholder="YYYY-MM-DD" required>
                        </div>
                    </div>
                </div>

                <button type="submit" name="submit" class="btn btn-primary btn-block">Film hinzufügen</button>

            </form>
        </div>
    </div>

    <div class="card box-shadow mb-4">
        <div class="card-header">
            Vorstellungen hinzufügen
        </div>
        <div class="card-body">
            <form method="post" action="?page=addvorstellung">
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    Beim hinzufügen einer Vorstellung wird eine mögloche vorhandene Vorstellung an dem Datum überschrieben.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label>Film auswählen</label>
                            <select class="form-control" name="film">
                                <?php
                                $res = $database->select("*","filme","","","");
                                while ( $tmp = mysqli_fetch_row($res)) {
                                    echo '<option value="'.$tmp[0].'">'.$tmp[6].'</option>';
                                }
                                mysqli_free_result($res);
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Kinosaal wählen</label>
                            <select class="form-control" name="saal">
                                <?php
                                $res = $database->select("*","kinos","","","");
                                while ( $tmp = mysqli_fetch_row($res)) {
                                    echo '<option value="'.$tmp[0].'">'.$tmp[2].'</option>';
                                }
                                mysqli_free_result($res);
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="mb-4">
                    <div class="row no-gutters justify-content-md-left">
                        <div class="col-md-auto">
                            <button disabled class="btn btn-outline-dark" style="border-radius: 0;">Zeiten</button><br/>
                            <button disabled class="btn btn-outline-dark" style="border-radius: 0;width: 100%;">14:00</button><br/>
                            <button disabled class="btn btn-outline-dark" style="border-radius: 0;width: 100%;">17:00</button><br/>
                            <button disabled class="btn btn-outline-dark" style="border-radius: 0;width: 100%;">20:00</button><br/>
                            <button disabled class="btn btn-outline-dark" style="border-radius: 0;width: 100%;">22:30</button><br/>
                        </div>

                        <?php
                            for($i = 0; $i < 9; $i++) {
                                echo '<div class="col-md-auto"><button disabled class="btn btn-outline-dark" style="border-radius: 0;">'.date("D d.m",strtotime("+".$i." day")).'</button><br/>';
                                for($x = 0; $x < 4;$x++) {

                                    echo '<div class="input-group-prepend"><div class="input-group-text" style="width: 100%;height: 38px;border-radius: 0;"><input type="checkbox" name="'.$x.'_'.date("d_m",strtotime("+".$i." day")).'" aria-label="Checkbox for following text input"></div></div>';
                                }
                                echo '</div>';

                            }

                        ?>

                    </div>
                </div>
                <button type="submit" name="submit" class="btn btn-primary btn-block">Speichern</button>

            </form>
        </div>
    </div>

    <div class="card box-shadow mb-4">
        <div class="card-header">
            Film löschen
        </div>
        <div class="card-body">
            <form method="post" action="?page=removefilm">
                <div class="row mb-2">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Film auswählen</label>
                            <select class="form-control" name="film">
                                <?php
                                $res = $database->select("*","filme","","","");
                                while ( $tmp = mysqli_fetch_row($res)) {
                                    echo '<option value="'.$tmp[0].'">'.$tmp[6].'</option>';
                                }
                                mysqli_free_result($res);
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <button type="submit" name="submit" class="btn btn-primary btn-block">Löschen</button>

            </form>
        </div>
    </div>

    <div class="card box-shadow" style="margin-bottom: 90px;">
        <div class="card-header">
            Datenbank aktionen
        </div>
        <div class="card-body">
            <form method="post" action="?page=dropvorstellungen">
                <button type="submit" name="submit" class="btn btn-danger btn-block">Alle Vorstellungen löschen</button>
            </form>
            <form method="post" action="?page=dropfilm">
                <button type="submit" name="submit" class="btn btn-danger btn-block">Alle Filme löschen</button>
            </form>
        </div>
    </div>
</div>