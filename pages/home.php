<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 15.07.2018
 * Time: 00:39
 */

?>
<div class="container" style="padding-top: 10px;padding-bottom: 35px;">

    <?php

        $res = $database->select("*","filme","","","");
        while ( $result = mysqli_fetch_row($res)) {
            ?>
                <div class="row">
                    <div class="col-md-2">
                        <div class="card mb-4 box-shadow">
                            <img class="card-img-top" alt="" src="img/<?php echo $result[1]; ?>" style="width: 100%; display: block;">
                            <div class="card-body" style="padding: 6px;">
                                <div class="row">
                                    <div class="col-md-6 text-left" style="padding-top: 3px;">
                                        <small class="text-muted"><?php echo $result[2]; ?> min</small>
                                    </div>
                                    <div class="col text-right">
                                        <?php
                                            $badge = "badge-primary";
                                            if ( $result[3] == "0") { $badge = "badge-secondary"; }
                                            if ( $result[3] == "6") { $badge = "badge-warning"; }
                                            if ( $result[3] == "12") { $badge = "badge-success"; }
                                            if ( $result[3] == "16") { $badge = "badge-primary"; }
                                            if ( $result[3] == "18") { $badge = "badge-danger"; }
                                            echo '<span class="badge '.$badge.'" style="margin-right: 3px;">'.$result[3].'</span>';

                                        ?>
                                        <span class="badge badge-dark"><?php echo $result[5]; ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="card mb-4 box-shadow">
                            <div class="card-header">
                                <?php echo $result[6]; ?>
                            </div>
                            <div class="card-body" style="min-height: 210px;padding: 0;padding-top: 9px;">
                                <div class="row no-gutters justify-content-md-center">
                                    <div class="col-md-auto">
                                        <button disabled class="btn btn-outline-dark" style="border-radius: 0;">Zeiten</button><br/>
                                        <button disabled class="btn btn-outline-dark" style="border-radius: 0;width: 100%;">14:00</button><br/>
                                        <button disabled class="btn btn-outline-dark" style="border-radius: 0;width: 100%;">17:00</button><br/>
                                        <button disabled class="btn btn-outline-dark" style="border-radius: 0;width: 100%;">20:00</button><br/>
                                        <button disabled class="btn btn-outline-dark" style="border-radius: 0;width: 100%;">22:30</button><br/>
                                    </div>
            <?php
                        for($i = 0; $i < 8; $i++) {
                            echo '<div class="col-md-auto"><button disabled class="btn btn-outline-dark" style="border-radius: 0;">'.date("D d.m",strtotime("+".$i." day")).'</button><br/>';
                            for($x = 0; $x < 4;$x++) {

                                // Prüfen ob eine Vorstellung vorhanden ist
                                $tmp = $database->select("*","vorstellungen",array("filmID='$result[0]'","tag='".date("Y-m-d",strtotime("+".$i." day"))."'","zeit='".$x."'"),"","");
                                if ( $tmp->num_rows > 0 ) {
                                    echo '<a class="btn btn-primary text-light" href="?page=vorstellung&id='.mysqli_fetch_row($tmp)[0].'" style="border-radius: 0;width: 100%;height: 38px;"><i class="fas fa-ticket-alt"></i></a><br/>';
                                } else {
                                    echo '<button disabled class="btn btn-outline-dark" style="border-radius: 0;width: 100%;">-</button><br/>';
                                }
                            }
                            echo '</div>';

                        }

            ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
        }

    ?>
