<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 15.07.2018
 * Time: 02:27
 */

include("movieHandler.php");

$movie = new movieHandler();
echo '<div class="container">';

$vars = array();
if(isset($_POST["submit"])) {
    try {
        foreach($_POST as $key => $post) {
            if ( $key != "film" && $key != "saal" && $key != "submit") {
                $vars[$key] = $post;
            }
        }

        // Sitzplätze aus Datenbank holen
        $res = $database->select("*","kinos",array('ID='.$_POST["saal"]),"","");
        $result = mysqli_fetch_row($res);
        $movie->addVorstellung($_POST["film"],$_POST["saal"],$result[1],$vars);
    } catch (Exception $e) {
        echo 'Exception abgefangen: ',  $e->getMessage(), "\n";
    } finally {
        echo '<div class="alert alert-success">Film erfolgreich hinzugefügt</div>';
    }

}

echo '</div>';
?>