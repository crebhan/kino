<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 15.07.2018
 * Time: 13:15
 */
if (isset($_GET["id"])) {$id = $_GET["id"]; }
$row = $database->select("*","vorstellungen",array("ID=".$id),"","");
$result = mysqli_fetch_row($row);
$row2 = $database->select("*","filme",array("ID='".$result[2]."'"),"","");
$result2 = mysqli_fetch_row($row2);

$time = "";
if ($result[6] == "0" ) { $time = "14:00"; }
if ($result[6] == "1" ) { $time = "17:00"; }
if ($result[6] == "2" ) { $time = "20:00"; }
if ($result[6] == "3" ) { $time = "22:30"; }
?>

<div class="container">
    <div class="card">
        <div class="card-header">
            <span class="float-left"><?php echo $result2[6]; ?></span>
            <span class="float-right"><?php echo $result[5]." ".$time; ?></span>
        </div>
        <form method="post" action="?page=makereservation">
        <div class="row">
            <div class="col-md-12">
                <div class="card-body" style="padding-bottom: 20px;">
                    <div class="card text-white bg-dark">
                        <div class="card-body text-center" style="padding: 3px;">
                            LEINWAND
                        </div>
                    </div>
                </div>
                <div style="padding-left: 20px;padding-right: 20px;">
                    <input type="hidden" name="vID" value="<?php echo $id; ?>">
                    <?php

                        $seats = explode(";",$result[3]);
                        $i = 0;
                        $x = 0;

                        foreach($seats as $rows) {
                            $tmp = explode(",",$rows);
                            echo '<div class="row">';

                                foreach($tmp as $tmp2) {

                                    if ( $tmp2 == "-" ) {
                                        echo '<div class="col"><div class="input-group-prepend" style="display: none;"><div class="input-group-text" style="width: 100%;height: 38px;border-radius: 0;"><input type="checkbox" disabled></div></div></div>';
                                    } else if ($tmp2 == "0") {
                                        echo '<div class="col"><div class="input-group-prepend"><div class="input-group-text" style="width: 100%;height: 38px;border-radius: 0;"><input type="checkbox" name="'.$i.'_'.$x.'"></div></div></div>';
                                    } else if ($tmp2 == "1") {
                                        echo '<div class="col"><div class="input-group-prepend"><div class="input-group-text" style="width: 100%;height: 38px;border-radius: 0;"><input type="checkbox" disabled></div></div></div>';
                                    }
                                    $x++;
                                }
                                $x = 0;
                                $i++;
                            echo '</div>';
                        }

                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div style="padding-left: 20px;padding-right: 20px;padding-top: 20px;">
                    <?php
                    if (isset($_SESSION["user"])) {
                        echo '<button type="submit" name="submit" class="btn btn-primary btn-block" >Ausgewähle Sitzplätze reservieren</button>';
                    } else {
                        echo '<div class="alert alert-warning">Bitte erstellen Sie ein Konto um Sitzplätze reservieren zu können</div>';
                    }
                    ?>
                </div>
            </div>
        </div>
        </form>

    </div>
</div>
