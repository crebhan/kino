<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 15.07.2018
 * Time: 14:27
 */
include("movieHandler.php");
$movie = new movieHandler();
?>

<div class="container">
    <div class="card">
        <div class="card-header">
            Ihre Reservierung
        </div>
        <div class="card-body">
            <div class="alert alert-success">
                Ihre Reservierung ist bei uns eingegangen. Bitte holen Sie ihre Karten spätestens <b>30 Minuten vor</b> Beginn der Vorstellung ab
            </div>

            <?php

                // sitze eintragen
                $row = $database->select("*","vorstellungen",array("ID=".$_POST["vID"]),"","");
                $result = mysqli_fetch_row($row);

                $movie->addReservation($_POST["vID"], $_SESSION["user"], $_POST, $result[3]);

            ?>
        </div>
    </div>
</div>
