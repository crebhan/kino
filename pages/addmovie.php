<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 15.07.2018
 * Time: 01:07
 */

include("movieHandler.php");

$movie = new movieHandler();
echo '<div class="container">';

    if(isset($_POST["submit"])) {
        try {
            $movie->addMovie($_POST["title"],$_POST["img"],$_POST["fsk"],$_POST["length"],$_POST["dimension"],$_POST["availableuntil"]);
        } catch (Exception $e) {
            echo 'Exception abgefangen: ',  $e->getMessage(), "\n";
        } finally {
            echo '<div class="alert alert-success">Film erfolgreich hinzugefügt</div>';
        }

    }

echo '</div>';
?>