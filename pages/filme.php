<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 15.07.2018
 * Time: 03:43
 */
?>
<div class="container">
    <div class="row">
        <?php

            $res = $database->select("*","filme","","","");
            while( $result = mysqli_fetch_row($res)) {
                $badge = "badge-primary";
                if ( $result[3] == "0") { $badge = "badge-secondary"; }
                if ( $result[3] == "6") { $badge = "badge-warning"; }
                if ( $result[3] == "12") { $badge = "badge-success"; }
                if ( $result[3] == "16") { $badge = "badge-primary"; }
                if ( $result[3] == "18") { $badge = "badge-danger"; }
               echo '<div class="col-md-2"><div class="card mb-4 box-shadow"><a href="?page=home"><img class="card-img-top card-movie" alt="" src="img/'.$result[1].'" style="width: 100%; display: block;"></a><div class="card-body" style="padding: 6px;"><div class="row"><div class="col-md-6 text-left" style="padding-top: 3px;"><small class="text-muted">'.$result[2].' min</small></div><div class="col text-right"><span class="badge '.$badge.'" style="margin-right: 3px;">'.$result[3].'</span><span class="badge badge-dark">'.$result[5].'</span></div></div></div></div></div>';
            }

        ?>
    </div>
</div>
