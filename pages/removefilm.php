<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 15.07.2018
 * Time: 12:04
 */

include("movieHandler.php");

$movie = new movieHandler();
echo '<div class="container">';

if(isset($_POST["submit"])) {
    try {
        $movie->deleteMovie($_POST["film"]);
    } catch (Exception $e) {
        echo 'Exception abgefangen: ',  $e->getMessage(), "\n";
    } finally {
        echo '<div class="alert alert-success">Film erfolgreich gelöscht</div>';
    }

}

echo '</div>';
?>