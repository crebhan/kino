<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 12.07.2018
 * Time: 18:46
 */
session_start();

    if(isset($_GET["page"])) {
        $page = $_GET["page"];
    } else {
        $page = "";
    }

    include("DatabaseHandler.php");
    $database = new DatabaseHandler('localhost','root','','kino');

?>
<html>
    <head>
        <title> Kino </title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="page.css" rel="stylesheet">
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    </head>
    <body>
        <div class="container" style="padding-top: 20px;padding-bottom: 35px;">
            <nav class="navbar navbar-expand-md secondNavbar" style="padding: 0;padding-left: .5rem;padding-right: 1rem;">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link secondNavItem active" href="?page=home">Kinoprogramm</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link secondNavItem" href="?page=filme">Aktuelle Filme &middot; <span style="font-weight: 700" class="text-primary"><?php echo $database->select("*","filme","","","")->num_rows; ?> </span></a>
                    </li>
                </ul>
                <ul class="navbar-nav mt-md-0 float-right">
                    <?php
                        if(isset($_SESSION['user'])) {
                            ?>
                            <li class="nav-item">
                                <?php
                                    $res = $database->select("*","user",array("ID=".$_SESSION["user"]),"","");
                                    $resu = mysqli_fetch_row($res);
                                ?>
                                <span class="nav-link secondNavItem disabled" href="#">Eingeloggt als, <span style="font-weight: 700"><?php echo $resu[1]; ?></span></span>
                            </li>
                            <li class="nav-item">
                                <?php
                                $res = $database->select("*","reservierungen",array("userID=".$_SESSION["user"]),"","");
                                $count = $res->num_rows;
                                ?>
                                <a class="nav-link secondNavItem" href="?page=reservierungen"> Meine Reservierungen/Buchungen &middot; <span style="font-weight: 700" class="text-primary"><?php echo $count; ?></span></a>
                            </li>
                            <?php

                                $row = $database->select("*","user",array("ID='".$_SESSION["user"]."'"),"","");
                                $tmp = mysqli_fetch_row($row);

                                if ( $tmp[3] > 0 ) {
                                    ?>
                                    <li class="nav-item">
                                        <a class="nav-link secondNavItem text-primary" href="?page=admin">
                                            Administration</a>
                                    </li>
                                    <?php
                                }
                                    ?>
                            <li class="nav-item">
                                <a class="nav-link secondNavItem text-danger" href="logout.php"> Logout</a>
                            </li>
                            <?php
                        } else {
                            ?>
                            <li class="nav-item">
                                <a class="nav-link secondNavItem text-primary" href="login.php"> Einloggen</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link secondNavItem text-dark" href="register.php"> Registrieren</a>
                            </li>
                    <?php
                        }
                    ?>
                </ul>
            </nav>
        </div>
        <!-- -->
        <!-- Main Content -->

        <?php

            // lade content abhängig von der aufgerufenen Seite
            if ($page == "") {
                $page = "home";
            }

            require_once("pages/".$page.".php");

        ?>

            <?php

                // PHP Code inkludieren abhängig von der Seite die aufgerufen wird


                // TODO:

            ?>
            <!-- -->
        </div>
        <footer class="footer">
            <p class="float-right"><span style="font-weight: 700">Kinosystem v. 1.0 &middot;</span> <span style="font-weight: 100">rev. 0f8653a</span></p>
            <p>&copy; 2018 Christoph Rebhan &middot; <a href="?page=impressum">Impressum</a></p>
        </footer>
    </body>
</html>
